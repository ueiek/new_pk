# config valid only for current version of Capistrano
lock '3.5.0'

set :stages, %w(development production)

set :application, 'api_pk_mgpu'
set :repo_url, 'git@bitbucket.org:royalsw/api_pk_mgpu.git'
# set :ssh_options, { :forward_agent => false }

set :linked_dirs, fetch(:linked_dirs, []).push('log', 'public/uploads', 'public/assets')
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# set :more_shared_settings, %w(secrets.yml)
# set :more_shared_children, %w(tmp/cache tmp/sockets vendor/bundle)
# require 'rollbar/capistrano'
# set :rollbar_token, '1231231231231231231221123123123123'
# set (:rollbar_env) { stage }

namespace :deploy do
  task :passenger_restart do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      execute "cd #{release_path} && touch tmp/restart.txt"
    end
    # run "supervisorctl restart mgpu:*"
  end

  # before :updated, :copy_configs do
  #   on roles(:web), in: :groups, limit: 3, wait: 10 do
  #     execute :cp, "/srv/mgpu/shared/config/database.yml #{release_path}/config/database.yml"
  #     execute :cp, "/srv/mgpu/shared/config/secrets.yml #{release_path}/config/secrets.yml"
  #   end
  # end
end


after "deploy:finished", "deploy:passenger_restart"