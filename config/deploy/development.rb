set :branch, 'master'
# set :domain,        'term.mgpu.ru'

server 'mgpu.royalsw.me', user: 'passenger', roles: %w{app db web resque_worker resque_scheduler}
set :rails_env, 'development'
set :resque_rails_env, 'development'
set :deploy_to, '/var/www/api_pk_mgpu.ru'
set :rvm_ruby_version, '2.2.0'
#

set :workers, {"stats" => 1}

#
# role :web,              "#{user}@#{domain}"
# role :app,              "#{user}@#{domain}"
# role :db,               "#{user}@#{domain}", :primary => true
# role :resque_worker,    "#{user}@#{domain}"
# role :resque_scheduler, "#{user}@#{domain}"