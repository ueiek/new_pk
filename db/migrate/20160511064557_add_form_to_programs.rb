class AddFormToPrograms < ActiveRecord::Migration
  def change
    remove_column :programs, :forms_id

    change_table :programs do |t|
      t.belongs_to :form, index: true
    end
  end
end
