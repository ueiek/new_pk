class AddQuotaToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :quota, :integer
  end
end
