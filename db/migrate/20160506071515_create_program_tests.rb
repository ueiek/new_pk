class CreateProgramTests < ActiveRecord::Migration
  def change
    create_table :program_tests do |t|
      t.belongs_to :program, index: true
      t.belongs_to :test, index: true

      t.timestamps null: false
    end
  end
end
