class MergeEnrollmetsToPrograms < ActiveRecord::Migration
  def change
    change_table :programs do |t|
      t.integer :current_point
      t.integer :previous_point
      t.integer :seat
    end
  end
end
