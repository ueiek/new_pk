class CreatePrograms < ActiveRecord::Migration
  def change
    create_table :programs do |t|
      t.belongs_to :forms, index: true
      t.belongs_to :descipline, index: true
      t.belongs_to :payment_type, index: true
      t.string :title

      t.timestamps null: false
    end
  end
end
