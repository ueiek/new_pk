class CreateEnrollments < ActiveRecord::Migration
  def change
    create_table :enrollments do |t|
      t.belongs_to :program, index: true
      t.integer :current_point
      t.integer :previous_point
      t.integer :joiners


      t.timestamps null: false
    end
  end
end
