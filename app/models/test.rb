# == Schema Information
#
# Table name: tests
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  points     :integer
#

class Test < ActiveRecord::Base
  has_many :program_tests, dependent: :destroy
  has_many :programs, through: :program_tests
end
