# == Schema Information
#
# Table name: programs
#
#  id              :integer          not null, primary key
#  descipline_id   :integer
#  payment_type_id :integer
#  title           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  current_point   :integer
#  previous_point  :integer
#  seat            :integer
#  quota           :integer
#  form_id         :integer
#

class Program < ActiveRecord::Base
  has_many :program_tests, dependent: :destroy
  has_many :tests, through: :program_tests
  belongs_to :form
  belongs_to :descipline
  belongs_to :payment_type


  scope :by_desc, -> (descipline_id) { where(descipline_id: descipline_id)}

  def test_titles
    self.tests.map(&:title).join(", ")

  end 
end
