# == Schema Information
#
# Table name: desciplines
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Descipline < ActiveRecord::Base
  has_many :programs
end
