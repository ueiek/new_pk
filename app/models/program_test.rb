# == Schema Information
#
# Table name: program_tests
#
#  id         :integer          not null, primary key
#  program_id :integer
#  test_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ProgramTest < ActiveRecord::Base
  belongs_to :program
  belongs_to :test
end
