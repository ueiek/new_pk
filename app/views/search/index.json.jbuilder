json.array!(@result) do |search|
  json.extract! search, :id
  json.url search_url(search, format: :json)
end
json.array! @results, partial: 'result', as: :result
