class DesciplinesController < ApplicationController
  before_action :set_descipline, only: [:show, :edit, :update, :destroy]

  # GET /desciplines
  # GET /desciplines.json
  def index
    @desciplines = Descipline.all
  end

  # GET /desciplines/1
  # GET /desciplines/1.json
  def show
  end

  # GET /desciplines/new
  def new
    @descipline = Descipline.new
  end

  # GET /desciplines/1/edit
  def edit
  end

  # POST /desciplines
  # POST /desciplines.json
  def create
    @descipline = Descipline.new(descipline_params)

    respond_to do |format|
      if @descipline.save
        format.json { render :show, status: :created, location: @descipline }
      else
        format.json { render json: @descipline.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /desciplines/1
  # PATCH/PUT /desciplines/1.json
  def update
    respond_to do |format|
      if @descipline.update(descipline_params)
        format.json { render :show, status: :ok, location: @descipline }
      else
        format.json { render json: @descipline.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /desciplines/1
  # DELETE /desciplines/1.json
  def destroy
    @descipline.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_descipline
      @descipline = Descipline.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def descipline_params
      params.require(:descipline).permit(:title)
    end
end
