class ProgramTestsController < ApplicationController
  before_action :set_program_test, only: [:show, :edit, :update, :destroy]

  # GET /program_tests
  # GET /program_tests.json
  def index
    if params[:program_id]
      program_id = params[:program_id].to_i
      @program_tests = ProgramTest.where(program_id: program_id) 
    else
      @program_tests = ProgramTest.all
    end
  end

  # GET /program_tests/1
  # GET /program_tests/1.json
  def show
  end

  # GET /program_tests/new
  def new
    @program_test = ProgramTest.new
  end

  # GET /program_tests/1/edit
  def edit
  end

  # POST /program_tests
  # POST /program_tests.json
  def create
    @program_test = ProgramTest.new(program_test_params)

    respond_to do |format|
      if @program_test.save
        format.json { render :show, status: :created, location: @program_test }
      else
        format.json { render json: @program_test.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /program_tests/1
  # PATCH/PUT /program_tests/1.json
  def update
    respond_to do |format|
      if @program_test.update(program_test_params)
        format.json { render :show, status: :ok, location: @program_test }
      else
        format.json { render json: @program_test.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /program_tests/1
  # DELETE /program_tests/1.json
  def destroy
    @program_test.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_program_test
      @program_test = ProgramTest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def program_test_params
      params.require(:program_test).permit(:program_id, :test_id)
    end
end
