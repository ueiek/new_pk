class SearchController < ApplicationController

  def index
    p "================="
    p params
    p params[:scores]
    p "================="
    #@degree = params[:degree_id] ? Degree.find(params[:degree_id]) : Degree.find_by(degree_code: '03')

    @score_params = params[:scores]
    @filter_open = true # if params[:scores]
    @programs = Program.all

    unless params[:scores].blank?
      unless @score_params[:payment_type_id].blank?
        @payment_type_id      = @score_params[:payment_type_id].to_i
        @programs = @programs.where("payment_type_id = ?", @payment_type_id )
      end

      unless @score_params[:descipline_id].blank?
        @descipline_id = @score_params[:descipline_id].to_i
        @programs = @programs.where("descipline_id = ?", @descipline_id )
      end

      unless @score_params[:form_id].blank?
        @form_id  = @score_params[:form_id].to_i
        @programs = @programs.where("form_id = ?", @form_id )
      end

      unless @score_params[:test_ids].blank?
        @test_ids           = @score_params[:test_ids].map(&:to_i)
        @test_ids.uniq!
        p @test_ids
        programs_arrays = @test_ids.map{|i| Test.find_by(id: i).programs}
        if @test_ids.length == 1
          @programs = []
        elsif @test_ids.length >= 2 && @test_ids.length <= 3
          programs_arrays.each do |p_a|
            @programs =  @programs.where("id IN (?)", p_a.map(&:id))
          end
          @programs
        elsif @test_ids.length >= 4
          @programs
          test_ids_by_3 = @test_ids.combination(3).to_a
          program_ids = []
          test_ids_by_3.each do |test_ids|
            programs_in_each = Program.all
            programs_arrays = test_ids.map{|i| Test.find_by(id: i).programs}
            programs_arrays.each do |p_a|
              program_ids.push((programs_in_each.where("id IN (?)", p_a.map(&:id))).map(&:id))
            end
          end
          program_ids = program_ids.flatten.uniq
          @programs = @programs.where("id IN (?)", program_ids)
        end
      end
    end
    render json: get_results(@programs)
  end

  private

  def get_results(programs)
    @desc_ids = programs.map { |x| x.descipline_id}
    @desciplines = Descipline.where("id IN (?)", @desc_ids)
    @results = []


    @desciplines.each do |d|
      data = programs.by_desc(d.id)
      @results.push({descipline: d, objects: get_objects(data), count: data.count })
    end
    @results
  end

  def get_objects(programs)
    programs.map { |x| {title: x.title, seat: x.seat, quota: x.quota, previous_point: x.previous_point, current_point: x.current_point, extra_info: x.payment_type.title + ', ' + x.form.title, tests: x.test_titles } }
  end

  def score_params
    params.require(:scores).permit(:payment_type_id, :descipline_id, :form_id, :test_ids)
  end


end

