require "rails_helper"

RSpec.describe DesciplinesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/desciplines").to route_to("desciplines#index")
    end

    it "routes to #new" do
      expect(:get => "/desciplines/new").to route_to("desciplines#new")
    end

    it "routes to #show" do
      expect(:get => "/desciplines/1").to route_to("desciplines#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/desciplines/1/edit").to route_to("desciplines#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/desciplines").to route_to("desciplines#create")
    end

    it "routes to #update" do
      expect(:put => "/desciplines/1").to route_to("desciplines#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/desciplines/1").to route_to("desciplines#destroy", :id => "1")
    end

  end
end
