require "rails_helper"

RSpec.describe ProgramTestsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/program_tests").to route_to("program_tests#index")
    end

    it "routes to #new" do
      expect(:get => "/program_tests/new").to route_to("program_tests#new")
    end

    it "routes to #show" do
      expect(:get => "/program_tests/1").to route_to("program_tests#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/program_tests/1/edit").to route_to("program_tests#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/program_tests").to route_to("program_tests#create")
    end

    it "routes to #update" do
      expect(:put => "/program_tests/1").to route_to("program_tests#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/program_tests/1").to route_to("program_tests#destroy", :id => "1")
    end

  end
end
