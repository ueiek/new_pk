require 'rails_helper'

RSpec.describe "program_tests/new", :type => :view do
  before(:each) do
    assign(:program_test, ProgramTest.new())
  end

  it "renders new program_test form" do
    render

    assert_select "form[action=?][method=?]", program_tests_path, "post" do
    end
  end
end
