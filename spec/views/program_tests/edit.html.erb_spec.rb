require 'rails_helper'

RSpec.describe "program_tests/edit", :type => :view do
  before(:each) do
    @program_test = assign(:program_test, ProgramTest.create!())
  end

  it "renders the edit program_test form" do
    render

    assert_select "form[action=?][method=?]", program_test_path(@program_test), "post" do
    end
  end
end
