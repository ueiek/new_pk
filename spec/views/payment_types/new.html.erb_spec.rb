require 'rails_helper'

RSpec.describe "payment_types/new", :type => :view do
  before(:each) do
    assign(:payment_type, PaymentType.new(
      :title => "MyString"
    ))
  end

  it "renders new payment_type form" do
    render

    assert_select "form[action=?][method=?]", payment_types_path, "post" do

      assert_select "input#payment_type_title[name=?]", "payment_type[title]"
    end
  end
end
