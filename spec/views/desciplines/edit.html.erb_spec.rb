require 'rails_helper'

RSpec.describe "desciplines/edit", :type => :view do
  before(:each) do
    @descipline = assign(:descipline, Descipline.create!(
      :title => "MyString"
    ))
  end

  it "renders the edit descipline form" do
    render

    assert_select "form[action=?][method=?]", descipline_path(@descipline), "post" do

      assert_select "input#descipline_title[name=?]", "descipline[title]"
    end
  end
end
