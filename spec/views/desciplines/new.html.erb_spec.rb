require 'rails_helper'

RSpec.describe "desciplines/new", :type => :view do
  before(:each) do
    assign(:descipline, Descipline.new(
      :title => "MyString"
    ))
  end

  it "renders new descipline form" do
    render

    assert_select "form[action=?][method=?]", desciplines_path, "post" do

      assert_select "input#descipline_title[name=?]", "descipline[title]"
    end
  end
end
