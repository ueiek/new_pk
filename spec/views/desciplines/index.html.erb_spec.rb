require 'rails_helper'

RSpec.describe "desciplines/index", :type => :view do
  before(:each) do
    assign(:desciplines, [
      Descipline.create!(
        :title => "Title"
      ),
      Descipline.create!(
        :title => "Title"
      )
    ])
  end

  it "renders a list of desciplines" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
  end
end
