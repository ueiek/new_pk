require 'rails_helper'

RSpec.describe "desciplines/show", :type => :view do
  before(:each) do
    @descipline = assign(:descipline, Descipline.create!(
      :title => "Title"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
  end
end
