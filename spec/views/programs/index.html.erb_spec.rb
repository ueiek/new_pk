require 'rails_helper'

RSpec.describe "programs/index", :type => :view do
  before(:each) do
    assign(:programs, [
      Program.create!(
        :title => "Title"
      ),
      Program.create!(
        :title => "Title"
      )
    ])
  end

  it "renders a list of programs" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
  end
end
