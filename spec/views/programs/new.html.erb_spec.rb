require 'rails_helper'

RSpec.describe "programs/new", :type => :view do
  before(:each) do
    assign(:program, Program.new(
      :title => "MyString"
    ))
  end

  it "renders new program form" do
    render

    assert_select "form[action=?][method=?]", programs_path, "post" do

      assert_select "input#program_title[name=?]", "program[title]"
    end
  end
end
