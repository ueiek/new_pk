require 'rails_helper'

RSpec.describe "forms/new", :type => :view do
  before(:each) do
    assign(:form, Form.new(
      :title => "MyString"
    ))
  end

  it "renders new form form" do
    render

    assert_select "form[action=?][method=?]", forms_path, "post" do

      assert_select "input#form_title[name=?]", "form[title]"
    end
  end
end
