require 'rails_helper'

RSpec.describe "forms/index", :type => :view do
  before(:each) do
    assign(:forms, [
      Form.create!(
        :title => "Title"
      ),
      Form.create!(
        :title => "Title"
      )
    ])
  end

  it "renders a list of forms" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
  end
end
