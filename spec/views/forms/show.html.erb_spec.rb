require 'rails_helper'

RSpec.describe "forms/show", :type => :view do
  before(:each) do
    @form = assign(:form, Form.create!(
      :title => "Title"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
  end
end
