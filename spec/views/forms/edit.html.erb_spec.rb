require 'rails_helper'

RSpec.describe "forms/edit", :type => :view do
  before(:each) do
    @form = assign(:form, Form.create!(
      :title => "MyString"
    ))
  end

  it "renders the edit form form" do
    render

    assert_select "form[action=?][method=?]", form_path(@form), "post" do

      assert_select "input#form_title[name=?]", "form[title]"
    end
  end
end
