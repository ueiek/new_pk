module Search
  # Выполняет поиск по направлениями, формам обучения и типу оплаты - бюджет\внебюджет

  # Дефолтные значения, которые позволяют выполнять поиск лишь по нескольким переданным параметрам
  DEGREE_ID = Degree.find_by(degree_code: "03").id
  EXAM_IDS = Exam.all.map(&:id)
  SPHERE_IDS = Sphere.all.map(&:id)
  COURSE_FORM_LABES = CourseForm::ROLE_LABELS.each_key.map{|k| k}
  COURSE_FORM_PRICE_LABELS = CourseForm::PRICE_LABELS.each_key.map{|k| k}

  class Programs

    attr_accessor :course_list, :results, :price_labels, :course_form_labels, :exam_ids, :sphere_ids

    def initialize(params = {degree_id: nil, exam_ids: anil, sphere_ids: nil, course_form_labels: nil, price_labels: nil})

      @degree_id          = params[:degree_id] || DEGREE_ID
      @exam_ids           = params[:exam_ids] ? params[:exam_ids].map(&:to_i) : []
      @sphere_ids         = params[:sphere_ids].compact.blank? ? SPHERE_IDS : params[:sphere_ids].compact
      @course_form_labels = params[:course_form_labels].compact.blank? ? COURSE_FORM_LABES : params[:course_form_labels].compact
      @price_labels       = params[:price_labels].compact.blank? ? COURSE_FORM_PRICE_LABELS : params[:price_labels].compact
      @results = []
      # result_items = [{sphere: Obj, objects: {object: Obj,
                      # places_count: Fixnum,
                      # last_year_scores: Fixnum,
                      # this_year_scores: Fixnum}},{}]
      calculate
    end

    def get_only_courses
      # Возвращаем из объекта только курсы: {object: Obj,
                                           # places_count: Fixnum,
                                           # last_year_scores: Fixnum,
                                           # this_year_scores: Fixnum}
      only_courses = []
      @results.each do |r|
        only_courses.push(r[:objects])
      end
      return only_courses.flatten
    end

    def get_filtering_info
      # Возвращаем массив со значениями переданных параметров фильтрации
      # максимумы захардкожены:
      #  -выводим Название сферы только если передана 1а сфера
      #  -выводим лейблы форм обучения только если передано 1 или 2 (<3)
      #  -выводим лейблы бюджет\не бюджет только если передана 1 (<2)
      results = []
      results.push(Degree.find(@degree_id).title)
      results.push(Sphere.find(@sphere_ids.last).title) if @sphere_ids.size==1
      if @course_form_labels.size < 3
        @course_form_labels.each do |label|
          results.push(CourseForm::ROLE_LABELS[label] + " форма")
        end
      end
      if @price_labels.size < 2
        @price_labels.each do |label|
          results.push(CourseForm::PRICE_LABELS[label])
        end
      end
      return results
    end

    private
    def calculate
      @course_list = Course.where("sphere_id IN (:spheres) AND degree_id = :degree", {spheres: @sphere_ids, degree: @degree_id})
      if @exam_ids.present?
        @course_list = @course_list.joins(:exams).where("exams.id IN (?)", @exam_ids).group("courses.id").having("COUNT(DISTINCT exams.id) = ?", @exam_ids.size)
      end
      genereate_results
      # На этом этапе мы получили список курсов который удовлетворяет введенным параметрам,
      # теперь наша задача - сгруппировать их c данными о баллах и местах на каждую из форм обучения
      # если не передана конкретная

    end

    def genereate_results
      Sphere.all.each do |sphere|
        @results.push({sphere: sphere, objects: []})
        @course_list.where(sphere_id: sphere.id).uniq.each do |course|
          # Генерируем результаты на основе переданных бюджет\внебюджет(@price_labels) и
          # форм обучения (@course_form_labels) с extra_info
          @course_form_labels.each do |label|
            @price_labels.each do |price|
              course_form = course.course_forms.by_price(price).by_role(label).try(:first)
              places_count = course_form.max_students_count if course_form
              @results.last[:objects].push({object: course, places_count: places_count, extra_info: generate_extra_info(label,price)}) if places_count
            end
          end
        end
        @results.pop if @results.last[:objects].blank?
      end
    end

    def generate_extra_info(course_form_label, price_label)
      # Генерируем дополнительную информацию по параметрам, фильтры по которым не были заданы
      # вывод может быть:
      #    -(форма обучения, бюджет/внебюджет)
      #    -(форма обучения)
      #    -(бюджет/внебюджет)
      result = []
      result.push(CourseForm::ROLE_LABELS[course_form_label]) if @course_form_labels.size > 1
      result.push(CourseForm::PRICE_LABELS[price_label]) if @price_labels.size > 1
      return result.blank? ? nil : "(#{result.join(', ')})"
    end
  end
end